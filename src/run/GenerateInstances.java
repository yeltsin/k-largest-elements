package run;

import java.io.File;
import java.io.IOException;

import utils.InstanceGenerator;

public class GenerateInstances {

	public static void main(String[] args) throws IOException {
		InstanceGenerator g;
		int[] size = { 100, 1000, 10000 };
		double[] rate = { 0.25, 0.50, 0.75 };
		int[] order = { -1, 0, 1 };
		String[] orderString = { "DESC", "NO_ORDER", "ASC" };
		for (int i = 0; i < size.length; i++) {
			for (int j = 0; j < rate.length; j++) {
				g = new InstanceGenerator(new File("in/I_" + size[i] + "_"
						+ orderString[j]
						+ ".dat"), order[j]);
				g.generate(size[i], 0, 50000);
			}
		}
	}

}
