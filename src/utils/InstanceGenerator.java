package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class InstanceGenerator {
	ArrayList<Integer> array;
	int size;
	File file;
	int order;
	public final int DECRESCENTE = -1;
	public final int CRESCENTE = 1;
	public final int NO_ORDER = 0;
	private Random random = new Random();
	
	public InstanceGenerator(File file, int order) {
		this.file = file;
		this.order = order;
		
	}
	
	public void generate(int size, int lowerBound, int upperBound) throws IOException {
		array = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++) {
			array.add(random.nextInt(upperBound-lowerBound)+lowerBound);
		}
		if(order == this.CRESCENTE) {
			Collections.sort(array);
		}
		if(order == this.DECRESCENTE) {
			Collections.sort(array, new Comparator<Integer>() {@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o2.compareTo(o1);
			}
			});
		}
		
		//Write File
		BufferedWriter buff = new BufferedWriter(new FileWriter(file));
		
		for (Integer integer : array) {
			buff.write(integer+" ");
		}
		
		buff.close();
	}
}
