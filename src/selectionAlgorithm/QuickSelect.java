package selectionAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class QuickSelect<T extends Comparable<T>> {
	ArrayList<T> array;

	public QuickSelect(ArrayList<T> elements) {
		array = elements;
	}
	
	public ArrayList<T> getLargestElements(int k) {
		KthLargest(0, array.size()-1, k);
		ArrayList<T>  arr= new ArrayList<T>(k);
		int count = 0;
		for (T t : array) {
			arr.add(t);
			if(++count == k) break;
		}
		
		Collections.sort(arr, new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				return o2.compareTo(o1);
			}
		});
		
		return arr;
	}
	
	private void KthLargest(int l, int r, int k) {

		if (k > 0 && k <= r - l + 1) {
			int pos = partition(l, r);

			if (pos - l == k - 1)
				array.get(pos);
			else if (pos - l > k - 1)
				KthLargest(l, pos - 1, k);
			else
				KthLargest(pos + 1, r, k - pos + l - 1);
		}
		
	}

	private int partition(int l, int r) {
		T x = array.get(r);
		int i = l;
		for (int j = l; j <= r - 1; j++) {
			if (array.get(j).compareTo(x) >= 0) {
				Collections.swap(array, j, i);
				i++;
			}
		}
		Collections.swap(array, i, r);
		return i;
	}
}
