package selectionAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
/**
 * 
 * @author italo
 * 
 * HeapTree or Priority Queue.
 * 
 * @param <T>
 */
@SuppressWarnings("rawtypes")
public class HeapTree <T extends Comparable>{
	ArrayList<T> heap = new ArrayList<T> ();
	
	public HeapTree() {
		heap.add(null);
	}
	
	public HeapTree(ArrayList<T> p) {
		heap.add(null);
		for (T t : p) {
			add(t);
		}
	}
	
	public ArrayList<T> getLargestElements(int k) {
		ArrayList<T> array = new ArrayList<T>(k);
		for (int i = 0; i < k; i++) {
			array.add(extractMax());
		}
		return array;
	}
	
	/**
	 * Add an object to the three
	 * @param object
	 */
	@SuppressWarnings("unchecked")
	public void add(T object) {
		heap.add(object);
		int index = heap.size()-1;
		int parent = getParent(index);
		
		while(true) {
			if(parent == 0) return;
			
			if(heap.get(index).compareTo(heap.get(parent)) > 0) {
				Collections.swap(heap, index, parent);
				index = parent;
				parent = getParent(index);
			} else {
				break;
			}
		}
	}
	/**
	 * 
	 * @return the largest object of the tree
	 */
	@SuppressWarnings("unchecked")
	public T extractMax() {
		if(empty()) return null;
		T root = heap.get(1);
		
		heap.set(1, heap.get(heap.size()-1));
		heap.remove(heap.size()-1);
		int index = 1;
		int largestChild;
		while(true) {
			largestChild = getLargestChild(index);
			if(largestChild == 0) break;
			if(heap.get(largestChild).compareTo(heap.get(index)) >= 1) {
				Collections.swap(heap, index, largestChild);
				index = largestChild;
			
			} else {
				break;
			}
		}
		
		return root;
	}
	/**
	 * 
	 * @param index
	 * @return the parent of given parameter.
	 */
	private int getParent(int index) {
		return index/2;
	}
	public boolean empty() {
		return heap.size() == 1;
		
	}
	/**
	 * 
	 * @param index
	 * @return the largest child of the given parameters.
	 * @return 0 if the index element has no child.
	 */
	@SuppressWarnings("unchecked")
	public int getLargestChild(int index) {
		int leftChild = 2*index;
		int rightChild = 2*index+1;
		
		if(leftChild >= heap.size() && rightChild >= heap.size()) {
			return 0;
		} else if(rightChild < heap.size()) {
			if(heap.get(leftChild).compareTo(heap.get(rightChild)) >= 1) {
				return leftChild;
			}else {
				return rightChild;
			}
		} else {
			return leftChild;
		}
	}
}
